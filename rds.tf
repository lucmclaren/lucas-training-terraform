resource "aws_security_group" "wordpress_db_sg" {
  name        = "wordpress-db-sg"
  description = "Wordpress db security group"
  vpc_id      = "${var.vpc_id}"

  ingress {
    security_groups  = ["${aws_security_group.wordpress_asg_sg.id}"]
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
  }
}

resource "aws_rds_cluster_instance" "wordpress_db_instances" {
  count                 = 3
  identifier            = "wordpres-db-cluster-node-${count.index}"
  cluster_identifier    = "${aws_rds_cluster.wordpress_db.id}"
  instance_class        = "db.t2.small"
  publicly_accessible   = false
  db_subnet_group_name  = "vpc-dev-web-app-defaultdbsubnetgroup-eefnhapnqndd"

}


resource "aws_rds_cluster" "wordpress_db" {
  cluster_identifier      = "wordpess-db-cluster"
  availability_zones      = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  database_name           = "${var.db_name}"
  master_username         = "${var.db_user}"
  master_password         = "${var.db_pass}"
  backup_retention_period = 7
  preferred_backup_window = "02:00-03:00"
  db_subnet_group_name    = "vpc-dev-web-app-defaultdbsubnetgroup-eefnhapnqndd"
  vpc_security_group_ids  = ["${aws_security_group.wordpress_db_sg.id}"]
}
