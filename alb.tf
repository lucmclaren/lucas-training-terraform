resource "aws_security_group" "wordpress_alb_sg" {
  name        = "wordpress-alb-sg"
  description = "Wordpress frontend loadbalancer security group"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port       = 80
    to_port         = 80
    cidr_blocks = ["0.0.0.0/0", "::/0"]
    protocol        = "tcp"
  }
}

resource "aws_alb_listener" "wordpress_alb_ls" {
  load_balancer_arn = "${aws_alb.wordpress_alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.wordpress_alb_tg.arn}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "wordpress_alb_tg" {
  name     = "wordpress-alb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
}

resource "aws_alb" "wordpress_alb" {
  name            = "wordpress-alb"
  internal        = false
  security_groups = ["${aws_security_group.wordpress_alb_sg.id}"]
  subnets         = ["${var.subnet_public_ids}"]
}
