
resource "aws_efs_file_system" "wordpress_efs" {
  creation_token = "wordpress_filessystem"
}

resource "aws_efs_mount_target" "wordpress_efs_az1" {
  file_system_id = "${aws_efs_file_system.wordpress_efs.id}"
  subnet_id      = "${var.subnet_public_az1_id}"
}

resource "aws_efs_mount_target" "wordpress_efs_az2" {
  file_system_id = "${aws_efs_file_system.wordpress_efs.id}"
  subnet_id      = "${var.subnet_public_az2_id}"
}

resource "aws_efs_mount_target" "wordpress_efs_az3" {
  file_system_id = "${aws_efs_file_system.wordpress_efs.id}"
  subnet_id      = "${var.subnet_public_az3_id}"
}
