variable "vpc_id" {
  type    = "string"
  default = "vpc-94e592f3"
}

variable "vpc_db_id" {
  type    = "string"
  default = "vpc-94e592f3"
}

variable "subnet_public_ids" {
  type      = "list"
  default   = ["subnet-c94ae392","subnet-e4dbb683","subnet-928afedb"]
}

variable "subnet_private_ids" {
  type      = "list"
  default   = ["subnet-eb49e0b0","subnet-32d5b855","subnet-798bff30"]
}

variable "subnet_public_az1_id" {
  type      = "string"
  default   = "subnet-c94ae392"
}

variable "subnet_public_az2_id" {
  type      = "string"
  default   = "subnet-e4dbb683"
}

variable "subnet_public_az3_id" {
  type      = "string"
  default   = "subnet-928afedb"
}

variable "db_name" {
  type      = "string"
  default   = "wordpress_db"
}

variable "db_user" {
  type      = "string"
  default   = "wordpress"
}

variable "db_pass" {
  type      = "string"
  default   = "put password here"
}
