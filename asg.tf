
resource "aws_security_group" "wordpress_asg_sg" {
  name        = "wordpress-asg-sg"
  description = "Wordpress autoscaling instance security group"

  vpc_id      = "${var.vpc_id}"

  ingress {
    security_groups  = ["${aws_security_group.wordpress_alb_sg.id}"]
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
  }
}


data "template_file" "wordpress_app_bootstrap" {
  template = "${file("${path.module}/wordpress_app_init.tpl")}"

  vars {
    environment     = "Dev"
    domain          = "acme.com"
    db_name         = "${var.db_name}"
    db_user         = "${var.db_user}"
    db_pass         = "${var.db_pass}"
    db_rds_endpoint = "${aws_rds_cluster.wordpress_db.endpoint}"
    efs_id          = "${aws_efs_file_system.wordpress_efs.id}"
    region          = "eu-west-1"
  }
}


data "template_cloudinit_config" "wordpress_app_config" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.wordpress_app_bootstrap.rendered}"
  }
}


resource "aws_launch_configuration" "wordpress_app" {
  name          = "wordpress_app"
  image_id      = "ami-ebd02392"
  instance_type = "t2.micro"
  user_data     = "${data.template_cloudinit_config.wordpress_app_config.rendered}"
}


resource "aws_autoscaling_group" "wordpress_app" {
  availability_zones        = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  name                      = "wordpress_app_asg"
  max_size                  = 6
  min_size                  = 3
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 3
  force_delete              = true
  launch_configuration      = "${aws_launch_configuration.wordpress_app.name}"
  vpc_zone_identifier       = "${var.subnet_private_ids}"
}
