provider "aws" {
  access_key = ""
  secret_key = ""
  region     = "eu-west-1"

  assume_role {
      role_arn     = "arn:aws:iam::243115555678:role/dev-web-app"
      session_name = "Terraform"
    }
}
