#!/bin/bash -x

## Prerequisites
yum update -y
yum install sudo wget python-pip -y
yum install httpd24 php70-mysqlnd php70 php70-mysql libapache2-mod-php70 php70-cli php70-cgi php70-gd mysql sendmail -y

## Mount EFS
mkdir -p /var/www/html/
EC2_AZ=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
echo \$EC2_AZ.${efs_id}.efs.${region}.amazonaws.com:/ /var/www/html/ nfs4 nfsvers=4.1rsize=1048576wsize=1048576hardtimeo=600retrans=2 0 0\ >> /etc/fstab mount -a
pip-2.6 install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz

## Install Wordpress
wget -qO- http://wordpress.org/latest.tar.gz | tar xvz -C /var/www/html/

## Configure Wordpress
cp /var/www/html/wordpress/wp-config-sample.php /var/www/html/wordpress/wp-config.php
sed -i "s/database_name_here/${db_name}/g"  /var/www/html/wordpress/wp-config.php
sed -i "s/username_here/${db_user}/g"       /var/www/html/wordpress/wp-config.php
sed -i "s/password_here/${db_pass}/g"       /var/www/html/wordpress/wp-config.php
sed -i "s/localhost/${db_rds_endpoint}/g"   /var/www/html/wordpress/wp-config.php

# exec > /tmp/userdata.log 2>&1

/bin/mv /var/www/html/wordpress/* /var/www/html/
/bin/rm -f /var/www/html/index.html
/bin/rm -rf /var/www/html/wordpress/
chown apache:apache /var/www/html/* -R
/usr/sbin/service apache2 restart
/usr/bin/curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
/bin/chmod +x wp-cli.phar
/bin/mv wp-cli.phar /usr/local/bin/wp
cd /var/www/html/
if ! $(sudo -u apache /usr/local/bin/wp core is-installed); then
  sudo -u apache /usr/local/bin/wp core install
          --url='wordpress.${domain}'
          --title='Cloudreach Meetup - ${environment}'
          --admin_user='root'
          --admin_password='wordpress'
          --admin_email='meetup@cloudreach.com'

  wget  https://s3-eu-west-1.amazonaws.com/sceptre-meetup-munich/header.jpg -O /var/www/html/wp-content/themes/twentyseventeen/assets/images/header.jpg
  chown apache:apache /var/www/html/wp-content/themes/twentyseventeen/assets/images/header.jpg
fi
